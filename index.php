<?php

include 'vendor/autoload.php';

// SCSS compiler

	use Leafo\ScssPhp\Compiler;

	$scss = new Compiler();
	$scss->setImportPaths("scss");
	$scss->setFormatter("Leafo\ScssPhp\Formatter\Expanded"); 
	//$scss->setFormatter("Leafo\ScssPhp\Formatter\Nested"); 
	//$scss->setFormatter("Leafo\ScssPhp\Formatter\Compressed");  
	//$scss->setFormatter("Leafo\ScssPhp\Formatter\Compact");  
	//$scss->setFormatter("Leafo\ScssPhp\Formatter\Crunched");

	$style = $scss->compile('@import "main.scss"');

	file_put_contents('css/main.css', $style);


// PUG compiler

	use Pug\Pug;

	$pug = new Pug(array(
	    'prettyprint' => true, 
	    'extension' => '.pug',
	    'cache' => 'cache/pug' 
	));

	$dirPugPages = array_diff(scandir('pug/pages'), ['..', '.']);
	$dirPugParts = array_diff(scandir('pug/parts'), ['..', '.']);

	if(!empty($dirPugPages)) {
		foreach ($dirPugPages as $value) {
			$outputParts = $pug->render('pug/pages/'. $value);
			file_put_contents('html/pages/'. substr($value, 0, -4) .'.html', $outputParts);
		}
	}

	if(!empty($dirPugParts)) {
		foreach ($dirPugParts as $value) {
			$outputParts = $pug->render('pug/parts/'. $value);
			file_put_contents('html/parts/'. substr($value, 0, -4) .'.html', $outputParts);
		}
	}

// Content render

	$url = ($_SERVER['REQUEST_URI']);

	if($url == '/' ) $url = '/index';

	if(file_exists('html/pages'. $url .'.html')) {
		$content = file_get_contents('html/pages'. $url .'.html');
	} else {
		echo 'Pug template not found';
	}

	$output = $pug->render('pug/main.pug', ['content' => '<?= $content ?>'] );
	file_put_contents('html/main.html', $output);


/////////////////////////////////////////////////////////////////////////

include 'html/main.html';



